# Cangelog

## 1.2.0
* [target/build] Add target_docker_build_* hooks
* [target/base] Add FROM_UBUNTU_VERSION Dockerfile argument

## 1.1.0
* [target/base] Remove Linux kernel image installation to be able to install a custom image on the final image targets

## 1.0.0
* Initial version
