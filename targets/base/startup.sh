#!/bin/bash -e
# LiveBOOTP startup script

. /etc/livebootp/functions.sh

for script in $(ls /etc/livebootp/startup.d/??_*.sh); do
    . $script
done

exit 0
